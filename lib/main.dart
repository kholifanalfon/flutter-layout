import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Scrollbar(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Center(
                child: Container(
                  padding: EdgeInsets.only(top: 50, bottom: 50, left: 30, right: 30),
                  child: Container(
                    color: Colors.white,
                    child: Row(
                      children: [
                        Expanded(child: Container(
                          child: Container(
                            padding: EdgeInsets.all(30),
                            child: Column(
                              children: [
                                Row(
                                    children: [
                                      Expanded(child: Container(
                                        padding: EdgeInsets.all(5.0),
                                        margin: EdgeInsets.only(bottom: 10.0),
                                        decoration: BoxDecoration(
                                            color: Colors.blueAccent.withOpacity(0.5),
                                            border: Border.all(color: Colors.grey)
                                        ),
                                        child: Text('Starawberry Pavlova', textAlign: TextAlign.center),
                                      ))
                                    ]
                                ),
                                Container(
                                  padding: EdgeInsets.all(5.0),
                                  margin: EdgeInsets.only(bottom: 10.0),
                                  decoration: BoxDecoration(
                                      color: Colors.blueAccent.withOpacity(0.5),
                                      border: Border.all(color: Colors.grey)
                                  ),
                                  child: Text('Pavlova is a merringue-based dessert named after the Russian ballerine Anna Pavlova. Pavlova featues a crisp crust and soft, light inside, topped with fruid and whipped cream.', textAlign: TextAlign.center),
                                ),
                                Container(
                                  padding: EdgeInsets.all(5.0),
                                  margin: EdgeInsets.only(bottom: 10.0),
                                  decoration: BoxDecoration(
                                      color: Colors.blueAccent.withOpacity(0.5),
                                      border: Border.all(color: Colors.grey)
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          Icon(Icons.star),
                                          Icon(Icons.star),
                                          Icon(Icons.star),
                                          Icon(Icons.star),
                                          Icon(Icons.star),
                                        ],
                                      ),
                                      Text('170 Reviews',
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: 'Roboto',
                                            letterSpacing: 0.5,
                                            fontSize: 12,
                                          ),
                                          textAlign: TextAlign.center
                                      ),
                                    ],
                                  ),
                                ),
                                DefaultTextStyle(
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w800,
                                      fontFamily: 'Roboto',
                                      letterSpacing: 0.5,
                                      fontSize: 18,
                                      height: 2,
                                    ),
                                    child: Container(
                                      padding: EdgeInsets.all(10),
                                      margin: EdgeInsets.only(bottom: 10.0),
                                      decoration: BoxDecoration(
                                          color: Colors.blueAccent.withOpacity(0.5),
                                          border: Border.all(color: Colors.grey)
                                      ),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Container(
                                            padding: EdgeInsets.only(left: 20, right: 20),
                                            child: Column(
                                              children: [
                                                Icon(Icons.kitchen, color: Colors.green[500]),
                                                DefaultTextStyle(
                                                    style: TextStyle(
                                                        fontSize: 12.0,
                                                        fontWeight: FontWeight.normal
                                                    ),
                                                    child: Column(
                                                      children: [
                                                        Text('PREP:', style: TextStyle(fontSize: 14.0)),
                                                        Text('25 min'),
                                                      ],
                                                    )
                                                )
                                              ],
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(left: 20, right: 20),
                                            child: Column(
                                              children: [
                                                Icon(Icons.timer, color: Colors.green[500]),
                                                DefaultTextStyle(
                                                    style: TextStyle(
                                                        fontSize: 12.0,
                                                        fontWeight: FontWeight.normal
                                                    ),
                                                    child: Column(
                                                      children: [
                                                        Text('COOK:', style: TextStyle(fontSize: 14.0)),
                                                        Text('1 hr'),
                                                      ],
                                                    )
                                                )
                                              ],
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(left: 20, right: 20),
                                            child: Column(
                                              children: [
                                                Icon(Icons.restaurant, color: Colors.green[500]),
                                                DefaultTextStyle(
                                                    style: TextStyle(
                                                        fontSize: 12.0,
                                                        fontWeight: FontWeight.normal
                                                    ),
                                                    child: Column(
                                                      children: [
                                                        Text('FEEDS:', style: TextStyle(fontSize: 14.0)),
                                                        Text('4-6'),
                                                      ],
                                                    )
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                )
                              ],
                            ),
                          ),
                        )),
                        Container(child: Image.network('https://3.bp.blogspot.com/-9SNrqTzyCqU/WZe52yqAPPI/AAAAAAAAOn8/A0wLHVcjYBIlHHE_DtB5IIh-8ZLOLIT0ACLcBGAs/s640/Pavlova%2Bby%2BSashy%2BLittle%2BKitchen%2B_08.JPG')),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),// This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
